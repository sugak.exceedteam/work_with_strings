`use strict`;

const string = `Я изучаю JavaScript`;

const paragraph = document.getElementById(`str`);
const btn = document.getElementById(`btn`);

let array = ['cat', 'dog', 'parrot', 'horse', 'fish', 'chicken', 'lion'];
let fourthArray = array;
//let newarray = array;
//let fourthArray = array;

paragraph.innerHTML = array;

//First variable
//array.splice(4, 1);

//Second variable
const secondVar = (newarray) => {
    let val1 = array.slice(0,4);
    let val2 = array.slice(5, array.length); 
    array = [];
    let resarray = array.concat(val1, val2);

    return resarray;
}

//Third variable
const thirdVar = (newarray) => {
    let temparray = [], result = [];
    const length = newarray.length - 1;

    for(let i = length; i >= 0; i--) {

        if(newarray[i] !== 'fish')
            temparray.push(newarray.pop());
        else {
            newarray.pop();
            result = newarray.concat(temparray.reverse());
            break;
        }
    } 
}

//Fourth variable
const fourthVar = () => {
    let myarray = []

    fourthArray.forEach((currentalue, index) => {
        if(currentalue !== 'fish') {
            myarray.push(currentalue);
        }
    });  

    return myarray;
}

btn.addEventListener(`click`, () => {alert(fourthVar());});
