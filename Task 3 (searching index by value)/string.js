'use strict';

const paragraph = document.getElementById('str');
const btn = document.getElementById('btn');
const array = ['cat', 'dog', 'parrot', 'horse'];

paragraph.innerHTML = array;

// First variable
let result = array.indexOf('parrot');

// Second variable
let newres = array.some(elem => elem === 'parrot');
if(newres) {
    for(let i = 0; i < array.length; i++) {
        if(array[i] === 'parrot') {
            newres = `index of parrot is: ${i}`;
            break;
        }
    }
} else newres = `Элемент не найден`;

//Third variable
let newres2 = array.findIndex(elem => elem === 'parrot');

btn.addEventListener('click',()=> {alert(newres2);});
